package xyz.mrdoomy.aunip.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.mrdoomy.aunip.models.*;
import xyz.mrdoomy.aunip.services.PizzaService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Api(tags = "Pizza(s)")
@RestController
@RequestMapping("/api")
public class PizzaController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PizzaService service;

    @ApiOperation(value = "Create Many Pizzas")
    @PostMapping("/pizzas")
    public ResponseEntity<CreateManyPayload> createManyPizzas(@RequestBody List<Pizza> pizzas) {
        LOGGER.debug("Add Many Pizzas");
        CreateManyPayload payload = new CreateManyPayload(pizzas.size());
        service.addManyPizzas(pizzas);
        return new ResponseEntity(payload, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Create Pizza")
    @PostMapping("/pizza")
    public ResponseEntity<CreatePayload> createPizza(@RequestBody Pizza pizza) {
        LOGGER.debug("Add Pizza");
        service.addPizza(pizza);
        CreatePayload payload = new CreatePayload(pizza.getId());
        return new ResponseEntity(payload, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Get All Pizzas")
    @GetMapping("/pizzas")
    public ResponseEntity<Collection<Pizza>> getAllPizzas() {
        LOGGER.debug("Find All Pizzas");
        Collection<Pizza> allPizzas = service.findAllPizzas();
        return new ResponseEntity(allPizzas, HttpStatus.OK);
    }

    @ApiOperation(value = "Get Pizza")
    @GetMapping("/pizza/{id}")
    public ResponseEntity<Optional<Pizza>> getPizza(@PathVariable(value = "id") String id) {
        LOGGER.debug("Find Pizza (With ID: {})", id);
        Optional<Pizza> optPizza = service.findPizza(id);

        if (optPizza.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(optPizza, HttpStatus.OK);
    }

    @ApiOperation(value = "Update Pizza")
    @PutMapping("/pizza/{id}")
    public ResponseEntity<UpdatePayload> updatePizza(@PathVariable(value = "id") String id, @RequestBody Pizza newPizza) {
        LOGGER.debug("Update Pizza (With ID: {})", id);
        Optional<Pizza> optPizza = service.findPizza(id);

        if (optPizza.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        Pizza pizza = optPizza.get();

        if (newPizza.getLabel() != null) {
            pizza.setLabel(newPizza.getLabel());
        }

        if (newPizza.getItems() != null) {
            pizza.setItems(newPizza.getItems());
        }

        if (newPizza.getPrice() != null) {
            pizza.setPrice(newPizza.getPrice());
        }

        service.updatePizza(pizza);
        UpdatePayload payload = new UpdatePayload(pizza.getId());
        return new ResponseEntity(payload, HttpStatus.OK);
    }

    @ApiOperation(value = "Clear All Pizzas")
    @DeleteMapping("/pizzas")
    public ResponseEntity<DeleteManyPayload> clearAllPizzas() {
        LOGGER.debug("Remove All Pizzas");
        Long count = service.countPizzas();
        DeleteManyPayload payload = new DeleteManyPayload(count);
        service.removeAllPizzas();
        return new ResponseEntity(payload, HttpStatus.OK);
    }

    @ApiOperation(value = "Remove Pizza")
    @DeleteMapping("/pizza/{id}")
    public ResponseEntity<DeletePayload> clearPizza(@PathVariable(value = "id") String id) {
        LOGGER.debug("Remove Pizza");
        Optional<Pizza> optPizza = service.findPizza(id);

        if (optPizza.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        service.removePizza(id);
        DeletePayload payload = new DeletePayload(id);
        return new ResponseEntity(payload, HttpStatus.OK);
    }
    
}
