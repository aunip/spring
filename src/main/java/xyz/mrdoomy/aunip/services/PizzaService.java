package xyz.mrdoomy.aunip.services;

import xyz.mrdoomy.aunip.models.Pizza;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface PizzaService {

    Long countPizzas();

    void addManyPizzas(List<Pizza> pizzas);

    void addPizza(Pizza pizza);

    Collection<Pizza> findAllPizzas();

    Optional<Pizza> findPizza(String id);

    Optional<Pizza> findPizzaByLabel(String label);

    void updatePizza(Pizza pizza);

    void removeAllPizzas();

    void removePizza(String id);

}
