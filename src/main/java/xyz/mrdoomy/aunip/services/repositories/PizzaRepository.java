package xyz.mrdoomy.aunip.services.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import xyz.mrdoomy.aunip.models.Pizza;

import java.util.Optional;

@Repository
public interface PizzaRepository extends MongoRepository<Pizza, String> {

    Optional<Pizza> findByLabel(String label);

}
