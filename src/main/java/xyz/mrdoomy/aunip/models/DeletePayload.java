package xyz.mrdoomy.aunip.models;

public class DeletePayload {

    private String deletedId;

    public DeletePayload(String deletedId) {
        this.deletedId = deletedId;
    }

    public String getDeletedId() {
        return deletedId;
    }

    public void setDeletedId(String deletedId) {
        this.deletedId = deletedId;
    }

    @Override
    public String toString() {
        return "DeletePayload (DeletedID: " + deletedId + ")";
    }
}
