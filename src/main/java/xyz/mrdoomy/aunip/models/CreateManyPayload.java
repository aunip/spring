package xyz.mrdoomy.aunip.models;

public class CreateManyPayload {

    private int createdCount;

    public CreateManyPayload(int createdCount) {
        this.createdCount = createdCount;
    }

    public int getCreatedCount() {
        return createdCount;
    }

    public void setCreatedCount(int createdCount) {
        this.createdCount = createdCount;
    }

    @Override
    public String toString() {
        return "CreateManyPayload (CreatedCount: " + createdCount + ")";
    }
}
