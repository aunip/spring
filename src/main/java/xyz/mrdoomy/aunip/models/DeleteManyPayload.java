package xyz.mrdoomy.aunip.models;

public class DeleteManyPayload {

    private Long deletedCount;

    public DeleteManyPayload(Long deletedCount) {
        this.deletedCount = deletedCount;
    }

    public Long getDeletedCount() {
        return deletedCount;
    }

    public void setDeletedCount(Long deletedCount) {
        this.deletedCount = deletedCount;
    }

    @Override
    public String toString() {
        return "DeleteManyPayload (DeletedCount: " + deletedCount + ")";
    }
}
