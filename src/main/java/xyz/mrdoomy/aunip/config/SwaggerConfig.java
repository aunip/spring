package xyz.mrdoomy.aunip.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("xyz.mrdoomy.aunip.controllers"))
            .paths(PathSelectors.regex("\\/api\\/pizza.*"))
            .build()
            .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("AUNIP API")
            .description("All U Need Is Pizza")
            .version("1.0.0")
            .contact(new Contact("MrDoomy", "https://www.mrdoomy.xyz", "mrdoomy@pm.me"))
            .license("Beerware")
            .licenseUrl("https://en.wikipedia.org/wiki/Beerware")
            .build();
    }
}
