package xyz.mrdoomy.aunip;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.mrdoomy.aunip.services.PizzaService;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class AunipApplicationTests {

	@Autowired
	PizzaService pizzaService;

	@Test
	void contextLoads() throws Exception {
		assertThat(pizzaService).isNotNull();
	}

}
